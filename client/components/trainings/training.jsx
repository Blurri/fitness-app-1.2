Training = React.createClass({
  mixins : [EnsureAuthenticated,ReactMeteorData],
  getMeteorData() {
    let training = Trainings.findOne({_id : this.props.trainingId,finished : false});
    return {
      training : training
    }
  },
  getInitialState() {
    return {
      currentIndex : 0
    }
  },
  nextExercise(finished,exerciseId,refs) {
    this.updateValues(refs,exerciseId);
    const {training} = this.data;
    const indexOfId = training.exercises.indexOf(exerciseId);
    let nextIndex = indexOfId < (training.exercises.length - 1) ? indexOfId + 1 : 0;
    if(finished){
      if(training.exercises.length === 1){
        Trainings.update(this.props.trainingId, {$pull : {exercises : exerciseId},$push : {finisedExercises : exerciseId},$set : {finished : true}});
        FlowRouter.go('/');
      }else{
        if(nextIndex === training.exercises.length - 1){
          nextIndex = 0;
        }
        Trainings.update(this.props.trainingId, {$pull : {exercises : exerciseId},$push : {finisedExercises : exerciseId}});
      }
    }
    this.setState({
      currentIndex : nextIndex
    })
  },
  updateValues(refs,exerciseId) {
    const {
      weightRef,
      durationRef,
      numberOfSetsRef,
      repsInSetRef,
      distanceRef
    } = refs;
    const updatedValues = {
      weight : weightRef != undefined ? weightRef.value : null,
      duration : durationRef != undefined ? durationRef.value : null,
      numberOfSets : numberOfSetsRef != undefined ? numberOfSetsRef.value : null,
      repsInSet : repsInSetRef != undefined ? repsInSetRef.value : null,
      distance : distanceRef != undefined ? distanceRef.value : null
    };
    Exercises.update(exerciseId, {$set : updatedValues}, (err, count) => {
      if(err) {alert('ERROR SAVING');return err};
    });
  },
  render() {
    const {training} = this.data;
    let exerciseId = training.exercises[this.state.currentIndex];
    let exercisesLeft = training.exercises.length;
    return (
      <div>
        <h1>TRAINING</h1>
        <AtFormReact />
        <TrainingCard onClick={this.nextExercise} exerciseId={exerciseId} exercisesLeft={exercisesLeft} />
      </div>
    )
  }
})
