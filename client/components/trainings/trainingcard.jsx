TrainingCard = React.createClass({
  mixins : [EnsureAuthenticated,ReactMeteorData],
  getMeteorData() {
    const exercise = Exercises.findOne({_id : this.props.exerciseId});
    return {
      exercise : exercise
    }
  },
  nextClick(finished,exerciseId,event){
    event.preventDefault();
    this.props.onClick(finished,exerciseId,this.refs);
  },
  render(){
    const {exercise} = this.data;
    return (
      <div className="card card-inverse">
        <div className="card-block">
          <h4 className="card-title">{exercise.title}<span className="label label-default pull-xs-right">{this.props.exercisesLeft} left</span></h4>
          <p className="card-text">{exercise.description}</p>
        </div>
        <ul className="list-group list-group-flush">
          {exercise.distance > 0 ? <li className="list-group-item clearfix">
            <input type="number" ref="distanceRef" defaultValue={exercise.distance} /> Distance
          </li> : null}
          {exercise.duration > 0 ? <li className="list-group-item clearfix">
            <input type="number" ref="durationRef" defaultValue={exercise.duration} /> / Minutes
          </li> : null}
          {exercise.weight > 0 ? <li className="list-group-item clearfix">
            <input type="number" ref="weightRef" defaultValue={exercise.weight} /> / Kg
          </li> : null}
          {exercise.numberOfSets > 0 ? <li className="list-group-item clearfix">
            <input type="number" ref="numberOfSetsRef" defaultValue={exercise.numberOfSets} /> Sets
          </li> : null}
          {exercise.repsInSet > 0 ? <li className="list-group-item clearfix">
            <input type="number" ref="repsInSetRef" defaultValue={exercise.repsInSet} /> Repetions in a set
          </li> : null}
        </ul>
        <div className="card-block">
          <div className="btn-toolbar" role="toolbar">
            <div className="btn-group" role="group">
              <button className="btn btn-primary" onClick={this.nextClick.bind(this,true,exercise._id)}>Done!</button>
            </div>
              <div className="btn-group" role="group">
                <button className="btn btn-primary" onClick={this.nextClick.bind(this,false,exercise._id)}>Another Exercise</button>
              </div>
          </div>
        </div>
      </div>
    )
  }
 })
