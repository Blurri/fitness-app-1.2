StatusimagesList = React.createClass({
  mixins: [EnsureAuthenticated, ReactMeteorData],
  getMeteorData() {
    const statusimages = Statusimages.find({userId : Meteor.userId()}).fetch();
    return {
      statusimages : statusimages
    }
  },
  showDetailView(_id) {
    FlowRouter.go('/statusimages/'+_id);
  },
  render() {
    return (
      <div className="statusimages-list">
        <div className="card-columns">
          {this.data.statusimages.map((entry,index)=>{
             return (
             <div className="card" key={index} >

              <h6 className="card-title">{moment(entry.createdAt).fromNow()}</h6>
              {entry.images ?
                <img className="card-img-top img-fluid" src={entry.images[0].url} alt="Card image cap" />
                :
                null
              }
              <div className="card-block">
                <p className="card-text">
                  {entry.comment}
                </p>
                <button className="btn btn-info btn-sm" onClick={this.showDetailView.bind(this,entry._id)}>Detail</button>
              </div>
            </div>)
          })}
        </div>
      </div>
    )
  }
})
