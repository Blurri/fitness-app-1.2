StatusimagesView = React.createClass({
  mixins: [EnsureAuthenticated, ReactMeteorData],
  getMeteorData() {
    const statusimage = Statusimages.findOne({_id : this.props.id});
    return {
      statusimage : statusimage
    }
  },
  render() {
    const entry = this.data.statusimage;
    return (
      <div className="statusimage-view">
        <h1>{moment(entry.createdAt).fromNow()}</h1>
        <p>{entry.comment}</p>
        <div className="row">
          {entry.images.map((image,index)=>{
            return (
              <div className="col-xs-3" key={index}>
                <img src={image.url} className="img-fluid" />
              </div>
            )
          })}
        </div>
      </div>
    )
  }
})
