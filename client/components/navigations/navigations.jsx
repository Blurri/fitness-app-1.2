
Navigations = React.createClass({
  mixins : [ReactMeteorData],
  getMeteorData() {
    return {
      currentUser: Meteor.user()
    }
  },
  logout(event){
    event.preventDefault();
    AccountsTemplates.logout();
    FlowRouter.go('/');
  },
  render() {



      return (
        <nav className="navbar navbar-light bg-faded top-nav">
          <a className="navbar-brand" href="/">FitnessApp</a>
          {this.data.currentUser ?
            <ul className="nav navbar-nav">
              <Navigation href="/plans" title="Plans" />
              <Navigation href="/exercises" title="Exercises" />
              <Navigation href="/new-plan" title="New Plan" />
              <Navigation href="/new-exercise" title="New Exercise" />
              <Navigation href="/userprofile" title="Profile" />
              <li className="nav-item">
                <a href="" className="nav-link" onClick={this.logout}>Logout</a>
              </li>
            </ul>
            :
            <ul className="nav navbar-nav">
              <Navigation href="/login" title="Login" />
            </ul>
          }
        </nav>
      )
  }
})
