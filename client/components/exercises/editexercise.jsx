EditExercise = React.createClass({
  mixins: [EnsureAuthenticated, ReactMeteorData],
  getMeteorData() {
    const exercise = Exercises.findOne({_id:this.props.exerciseId})
    return {
      exercise : exercise
    }
  },
  render() {
    const {exercise} = this.data;
    return(
      <div className="edit-exercise">
        <form>
          <div className="row">
            <div className="col-xs-9">
              <fieldset className="form-group input-group-sm">
                <label>Title</label>
                <input ref="titleRef" className="form-control" type="Text" defaultValue={exercise.title} placeholder="Enter your exercise title." />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Type</label>
                <select className="form-control" ref="typeRef" defaultValue={exercise.type} >
                  <option value="stammina">stammina</option>
                  <option value="strength">strength</option>
                  <option value="stretch">stretch</option>
                </select>
              </fieldset>
            </div>
          </div>
          <hr/>
          <div className="row">
            <div className="col-xs-12">
              <fieldset className="form-group input-group-sm">
                <label>description</label>
                <textarea ref="descriptionRef" className="form-control" defaultValue={exercise.description} rows="5" ></textarea>
              </fieldset>
            </div>
          </div>
          <hr/>
          <div className="row">
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Duration</label>
                <input type="number" className="form-control" defaultValue={exercise.duration} placeholder="Duration" ref="durationRef"  />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Distance</label>
                <input type="number" placeholder="Distance" ref="distanceRef" className="form-control" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Weight</label>
                <input className="form-control" type="number" defaultValue={exercise.weight} placeholder="Weight" ref="weightRef"  />
              </fieldset>
            </div>
          </div>
          <hr/>
          <div className="row">
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label># of sets</label>
                <input type="number" className="form-control" defaultValue={exercise.numberOfSets} placeholder="# of Sets" ref="numberOfSetsRef" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label># of repetions in a set</label>
                <input type="number" className="form-control" defaultValue={exercise.repsInSet} placeholder="# of Sets" ref="repsInSetRef"  />
              </fieldset>
            </div>
          </div>
          <hr/>
          <button className="btn" onClick={this.updateExercise}>Update</button>
        </form>
      </div>
    )
  },
  updateExercise(event) {
    event.preventDefault();
    const {exercise} = this.data;

    const {titleRef,typeRef,weightRef,durationRef,numberOfSetsRef,repsInSetRef,descriptionRef,distanceRef} = this.refs;
    let uV = {title : titleRef.value,type : typeRef.value,weight : weightRef.value,duration : durationRef.value,numberOfSets : numberOfSetsRef.value,repsInSet : repsInSetRef.value,description : descriptionRef.value,distance : distanceRef.value};
    exercise.weight ? null :  (weightRef.value ? uV.start_weight = {value : weightRef.value, createdAt : new Date()} :null);
    exercise.duration ? null :  (durationRef.value ? uV.start_duration = {value : durationRef.value, createdAt : new Date()} :null);
    exercise.distance ? null :  (distanceRef.value ? uV.start_distance = {value : distanceRef.value, createdAt : new Date()} :null);
    exercise.numberOfSets ? null :  (numberOfSetsRef.value ? uV.start_numberOfSets = {value : numberOfSetsRef.value, createdAt : new Date()} :null);
    exercise.repsInSet ? null :  (repsInSetRef.value ? uV.start_repsInSet = {value : repsInSetRef.value, createdAt : new Date()} :null);


    Exercises.update(this.data.exercise._id, {$set : updatedValues}, (err, count) => {
      if(err) {alert('ERROR SAVING');return err};
      FlowRouter.go('/exercise/'+this.data.exercise._id);
    });
  }
})
