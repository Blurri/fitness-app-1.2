NewExercise = React.createClass({
  mixins : [EnsureAuthenticated],
  render() {
    return(
      <div className="new-exercise">
        <h2>Add New Exercise</h2>
        <form>
          <div className="row">
            <div className="col-xs-9">
              <fieldset className="form-group input-group-sm">
                <label>Exercise Title</label>
                <input ref="titleRef" type="Text" placeholder="Enter your exercise title." className="form-control" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Example select</label>
                <select className="form-control" ref="typeRef" defaultValue="stammina">
                  <option value="stammina">stammina</option>
                  <option value="strength">strength</option>
                  <option value="stretch">stretch</option>
                </select>
              </fieldset>
            </div>
          </div>

          <hr/>
          <div className="row">
            <div className="col-xs-12">
              <fieldset className="form-group input-group-sm">
                <label>Description</label>
                <textarea ref="descriptionRef" rows="5" className="form-control" ></textarea><br/>
              </fieldset>
            </div>
          </div>
          <hr/>

          <div className="row">
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Duration</label>
                <input type="number" placeholder="Duration" ref="durationRef" className="form-control" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Distance</label>
                <input type="number" placeholder="Distance" ref="distanceRef" className="form-control" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label>Weight</label>
                <input type="number" placeholder="Weight" ref="weightRef" className="form-control" />
              </fieldset>
            </div>
          </div>
          <hr/>
          <div className="row">
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label># of Sets</label>
                <input type="number" placeholder="# of Sets" ref="numberOfSetsRef" className="form-control" />
              </fieldset>
            </div>
            <div className="col-xs-3">
              <fieldset className="form-group input-group-sm">
                <label># of repetions in a set</label>
                <input type="number" placeholder="# of Sets" ref="repsInSetRef" className="form-control" />
              </fieldset>
            </div>
          </div>
          <hr/>
          <button onClick={this.createExercise} className="btn">Add New</button>
        </form>
      </div>
    )
  },
  createExercise(event) {
    event.preventDefault();
    const {
      titleRef,
      typeRef,
      weightRef,
      durationRef,
      numberOfSetsRef,
      repsInSetRef,
      descriptionRef,
      distanceRef
    } = this.refs;
    const iOb = {title : titleRef.value,type : typeRef.value,description : descriptionRef.value,
      userId : Meteor.userId(),distance : distanceRef.value,numberOfSets : numberOfSetsRef.value,
      duration : durationRef.value,weight : weightRef.value}
    weightRef.value ? iOb.start_weight = {value : weightRef.value, createdAt : new Date()} : null;
    durationRef.value ? iOb.start_duration = {value : durationRef.value, createdAt : new Date()} : null;
    numberOfSetsRef.value ? iOb.start_numberOfSets = {value : numberOfSetsRef.value, createdAt : new Date()} : null;
    repsInSetRef.value ? iOb.start_repsInSet = {value : repsInSetRef.value, createdAt : new Date()} : null;
    distanceRef.value ? iOb.start_distance = {value : distanceRef.value, createdAt : new Date()} : null;
    Exercises.insert(
      iOb
    , function (err, exerciseId) {
      if(err){
        alert('ERROR SAVING');
        return err;
      }
      FlowRouter.go('/exercise/'+exerciseId);
    })
  }
})
