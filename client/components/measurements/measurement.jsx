Measurement = React.createClass({
  mixins : [EnsureAuthenticated,ReactMeteorData],
  getMeteorData() {
    if(this.props.showlatest){
      const measurement = Measurements.findOne({user : Meteor.userId()}, {sort: {createdAt: -1}, limit: 1});
      return {
        measurement : measurement || {}
      }
    }
  },
  render() {
    const {measurement} = this.data;
    if(!measurement._id){return(<div></div>)};
    let createdAt = moment(measurement.createdAt);
    return (
      <div className="measurement">
        <h3>Latest measurement from {createdAt.fromNow()}</h3>
        <hr/>
        <div className="row">
          <div className="col-xs-12">
            <h4>Girth</h4>
            <div className="row">
              <div className="col-xs-3">
                <p>Upper arm {measurement.upperarm} cm</p>
              </div>
              <div className="col-xs-3">
                <p>Chest {measurement.chest} cm</p>
              </div>
              <div className="col-xs-3">
                <p>Waist {measurement.waist} cm</p>
              </div>
              <div className="col-xs-3">
                <p>Thigh {measurement.thigh} cm</p>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-3">
            <h4>Weight</h4>
            <p>Weight {measurement.weight} kg</p>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <div className="btn-group pull-xs-left">
              <button onClick={this.editMeasurement} className="btn btn-success">
                Edit
              </button>
            </div>
            <div className="btn-group pull-xs-right">
              <a className="btn btn-info" href="/measurementlist">Historie</a>
            </div>
          </div>
        </div>
      </div>
    )
  },
  editMeasurement(event) {
    event.preventDefault();
    FlowRouter.go('/edit-measurement/'+this.data.measurement._id);
  }
})
