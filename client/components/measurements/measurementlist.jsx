MeasurementList = React.createClass({
  mixins : [EnsureAuthenticated,ReactMeteorData],
  getMeteorData() {
    const measurements = Measurements.find({user : Meteor.userId()}, {sort : {createdAt : -1}}).fetch();
    return {
      measurements : measurements
    }
  },
  render() {
    return (
      <div createClass="measurement-list">
        <h1>Measurement Hirtorie</h1>

          <div className="row">
            <div className="col-md-12">
              <table className="table table-sm">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>When</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.data.measurements.map((measurement, index) => (
                    <tr key={measurement._id}>
                      <td>{index+1}</td>
                      <td>{moment(measurement.createdAt).fromNow()}</td>
                      <td>
                        <a href={'/edit-measurement/'+measurement._id}>Edit</a> |
                        <a href={'/measurement/'+measurement._id}>View</a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
      </div>
    )
  }
})
