EditPlan = React.createClass({
  mixins : [EnsureAuthenticated,ReactMeteorData],
  getMeteorData() {
    const plan = Plans.findOne({_id : this.props.planId})
    return {
      plan : plan
    }
  },
  render() {
    return(
      <div className="edit-plan">
        <form>
          <div className="row">
            <div className="col-xs-9">
              <fieldset className="form-group input-group-sm">
                <label>Title</label>
                <input ref="titleRef" className="form-control" type="Text" defaultValue={this.data.plan.title} placeholder="Enter your exercise title." />
              </fieldset>
            </div>
          </div>
          <hr/>
          <button className="btn" onClick={this.updatePlan}>Update</button>
        </form>

        <hr/>

        <PlanExerciseList planId={this.data.plan._id} />

      </div>
    )
  },
  updatePlan(event){
    event.preventDefault();
  }
})
