NewPlan = React.createClass({
  mixins : [EnsureAuthenticated],
  render() {
    return (
      <div className="new-plan">
        <h2>Add New Plan</h2>
        <form>
          <fieldset className="form-group">
            <label>Plan Title</label>
            <input className="form-control" ref="titleRef" type="Text" placeholder="Enter your plan title." />
          </fieldset>
          <button className="btn btn-default" onClick={this.createPlan}>Add New</button>
        </form>
      </div>
    )
  },
  createPlan(event){
    event.preventDefault();
    const {titleRef} = this.refs;
    Plans.insert({title : titleRef.value, trainings : [], exercises : [], userId : Meteor.userId()},(err,id) => {
      if(err){return err;}
      FlowRouter.go('/edit-plan/'+id);
    });
  }
})
