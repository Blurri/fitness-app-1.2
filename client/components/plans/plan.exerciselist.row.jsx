PlanExerciseListRow = React.createClass({
  mixins : [EnsureAuthenticated,ReactMeteorData],
  getMeteorData() {
    const plan = Plans.findOne({_id:this.props.planId});
    const exercise = Exercises.findOne({_id:this.props.exerciseId});
    return {
      exercise : exercise || {},
      plan : plan || {}
    }
  },
  addExercise(event) {
    event.preventDefault();
    Plans.update(this.props.planId, {$addToSet : {exercises : this.props.exerciseId}}, (err,count) => {
      if(err){alert('UPDATE FAILED'); return err;}
    })
  },
  removeExercise(event) {
    event.preventDefault();
    Plans.update(this.props.planId, {$pull : {exercises : this.props.exerciseId}}, (err,count) => {
      if(err){alert('UPDATE FAILED'); return err;}
    })
  },
  render() {
    const {exercise,plan} = this.data;
    let className = 'success';
    let buttonText = 'add';
    let action = this.addExercise;
    plan.exercises.map((id) => {
      if(id === exercise._id){
        className = 'danger';
        buttonText = 'remove';
        action = this.removeExercise;
      }
    });
    return (
      <tr>
        <td>{this.props.index+1}</td>
        <td>{exercise.title}</td>
        <td>{exercise.type}</td>
        <td>
            <button className={'btn btn-sm btn-' + className} onClick={action} >{buttonText}</button>
            <a className="btn btn-link btn-sm" href={'/exercises/'+exercise._id}><i className="fa fa-eye"></i></a>
        </td>
      </tr>
    )
  }
})
