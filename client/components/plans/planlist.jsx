PlanList = React.createClass({
  mixins : [EnsureAuthenticated,ReactMeteorData],
  getMeteorData() {
    const plans = Plans.find().fetch();
    return {
      plans : plans
    }
  },
  render() {
    return(
      <div className="row">
        <div className="col-md-12">
          <table className="table table-sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.data.plans.map((plan, index) => (
                <tr key={plan._id}>
                  <td>{index+1}</td>
                  <td>{plan.title}</td>
                  <td>
                    <a href={'/edit-plan/'+plan._id}><i className="fa fa-eye"></i></a> |
                    <a href="#" onClick={this.createNewTraining.bind(this,plan)} ><i className="fa fa-step-forward"></i></a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    )
  },
  createNewTraining(plan,event){
    event.preventDefault();
    let training = Trainings.findOne({planId : plan._id, finished : false});
    if(training == undefined){
      Trainings.insert({planId:plan._id, finished : false, exercises : plan.exercises , finisedExercises : [], userId : Meteor.userId()},(err,_id) => {
        if(err){alert('error');return err;}
        FlowRouter.go('/training/'+_id);
      })
    }else{
      FlowRouter.go('/training/'+training._id);
    }
  }


})
