EditProfileAvatar = React.createClass({
  render() {
    return (
      <div className="row">
        <div className="card">
          <div className="card-block">
            {this.props.avatar ?
              <img className="img-rounded img-fluid avatar" src={this.props.avatar} alt="profilepicture" />
              :
              <i className="fa fa-user fa-6 text-xs-center"></i>
            }
            <span className="btn btn-info btn-sm btn-file">
                change <input type="file" className="file_bag" onChange={this.props.didSelectImage} accept="image/x-png, image/gif, image/jpeg" />
            </span>

          </div>
          <div className="card-block">
            <h4 className="card-title">{this.props.username}</h4>
            <p className="card-text">{this.props.email}</p>
          </div>
        </div>
      </div>
    )
  }
})
